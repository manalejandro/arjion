FROM debian:stable-slim
RUN apt-get update
RUN apt-get -y upgrade
RUN mkdir /upload /usr/share/man/man1 /usr/share/man/man8
RUN apt-get -y install --no-install-recommends apt apt-transport-https apt-utils readline-common curl gnupg software-properties-common dirmngr openjdk-8-jdk procps
RUN echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" > /etc/apt/sources.list.d/elastic-6.x.list
RUN apt-key adv --recv-keys D27D666CD88E42B4
RUN apt-get update
RUN apt-get -y install --no-install-recommends maven tesseract-ocr tesseract-ocr-spa elasticsearch git
RUN sed -i "s/#cluster.name: my-application/cluster.name: elasticsearch/" /etc/elasticsearch/elasticsearch.yml
RUN git clone https://gitlab.com/manalejandro/arjion
RUN mvn clean install -f /arjion/pom.xml
RUN echo "/etc/init.d/elasticsearch start && mvn spring-boot:run -f /arjion/pom.xml" > entrypoint.sh
EXPOSE 8080:8080
ENTRYPOINT ["bash", "entrypoint.sh"]
