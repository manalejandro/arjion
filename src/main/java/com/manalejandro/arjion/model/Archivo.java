package com.manalejandro.arjion.model;

import java.util.Map;

public class Archivo {

    private String nombre;
    private Integer tamano;
    private Map metadata;
    private String contenido;
    private String lenguaje;

    public Archivo(String nombre, Integer tamano, Map metadata, String contenido, String lenguaje) {
        this.nombre = nombre;
        this.tamano = tamano;
        this.metadata = metadata;
        this.contenido = contenido;
        this.lenguaje = lenguaje;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @return the tamano
     */
    public Integer getTamano() {
        return tamano;
    }

    /**
     * @return the metadata
     */
    public Map getMetadata() {
        return metadata;
    }

    /**
     * @return the contenido
     */
    public String getContenido() {
        return contenido;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @param tamano the tamano to set
     */
    public void setTamano(Integer tamano) {
        this.tamano = tamano;
    }

    /**
     * @param metadata the metadata to set
     */
    public void setMetadata(Map metadata) {
        this.metadata = metadata;
    }

    /**
     * @param contenido the contenido to set
     */
    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    /**
     * @return the lenguaje
     */
    public String getLenguaje() {
        return lenguaje;
    }

    /**
     * @param lenguaje the lenguaje to set
     */
    public void setLenguaje(String lenguaje) {
        this.lenguaje = lenguaje;
    }
}