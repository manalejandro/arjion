package com.manalejandro.arjion.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Mapping;
import org.springframework.data.elasticsearch.annotations.Setting;

@Document(indexName = "#{@indexName}", type = "#{@documentType}")
@Setting(settingPath = "/elasticsearch/settings.json")
@Mapping(mappingPath = "/elasticsearch/mapping.json")
public class Documento {
    @Id
    public String nombre;
    public Integer tamano;
    public JsonNode metadata;
    public String contenido;
    public String lenguaje;

    @JsonCreator
    public Documento(@JsonProperty("nombre") String nombre, @JsonProperty("tamano") Integer tamano,
            @JsonProperty("metadata") JsonNode metadata, @JsonProperty("contenido") String contenido,
            @JsonProperty("lenguaje") String lenguaje) {
        this.nombre = nombre;
        this.tamano = tamano;
        this.metadata = metadata;
        this.contenido = contenido;
        this.lenguaje = lenguaje;
    }

    /**
     * @return the nombre
     */
    @JsonProperty("nombre")
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the tamano
     */
    @JsonProperty("tamano")
    public Integer getTamano() {
        return tamano;
    }

    /**
     * @param tamano the tamano to set
     */
    public void setTamano(Integer tamano) {
        this.tamano = tamano;
    }

    /**
     * @return the metadata
     */
    @JsonProperty("metadata")
    public JsonNode getMetadata() {
        return metadata;
    }

    /**
     * @param metadata the metadata to set
     */
    public void setMetadata(JsonNode metadata) {
        this.metadata = metadata;
    }

    /**
     * @return the contenido
     */
    @JsonProperty("contenido")
    public String getContenido() {
        return contenido;
    }

    /**
     * @param contenido the contenido to set
     */
    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    /**
     * @return the lenguaje
     */
    @JsonProperty("lenguaje")
    public String getLenguaje() {
        return lenguaje;
    }

    /**
     * @param lenguaje the lenguaje to set
     */
    public void setLenguaje(String lenguaje) {
        this.lenguaje = lenguaje;
    }
}
