package com.manalejandro.arjion.model;

import java.util.ArrayList;
import java.util.List;

public class Consulta {
    private List<Documento> documentos = new ArrayList<Documento>();
    private String suggest;
    private List<String> autocomplete = new ArrayList<String>();

    /**
     * @return the documentos
     */
    public List<Documento> getDocumentos() {
        return documentos;
    }

    /**
     * @return the suggest
     */
    public String getSuggest() {
        return suggest;
    }

    /**
     * @return the autocomplete
     */
    public List<String> getAutocomplete() {
        return autocomplete;
    }

    /**
     * @param documentos the documentos to set
     */
    public void setDocumentos(List<Documento> documentos) {
        this.documentos = documentos;
    }

    /**
     * @param suggest the suggest to set
     */
    public void setSuggest(String suggest) {
        this.suggest = suggest;
    }

    /**
     * @param autocomplete the autocomplete to set
     */
    public void setAutocomplete(List<String> autocomplete) {
        this.autocomplete = autocomplete;
    }
}