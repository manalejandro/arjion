package com.manalejandro.arjion;

import java.net.InetAddress;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = "com.manalejandro.arjion.repositories")
public class ESConfig {

	@Value("${elasticsearch.host}")
	private String EsHost;

	@Value("${elasticsearch.port}")
	private int EsPort;

	@Value("${elasticsearch.clustername}")
	private String EsClusterName;

	@Value("${elasticsearch.nodename}")
	private String EsNodeName;

	@Value("${arjion.indexName}")
	private String indexName;

	@Value("${arjion.documentType}")
	private String documentType;

	private TransportClient client;

	@Bean
	public Client client() throws Exception {

		Settings esSettings = Settings.builder().put("cluster.name", EsClusterName).put("node.name", EsNodeName)
				.build();

		client = new PreBuiltTransportClient(esSettings);
		return client.addTransportAddress(new TransportAddress(InetAddress.getByName(EsHost), EsPort));
	}

	@Bean
	public ElasticsearchOperations elasticsearchTemplate() throws Exception {
		return new ElasticsearchTemplate(client());
	}

	@Bean
	public String indexName() {
		return indexName;
	}

	@Bean
	public String documentType() {
		return documentType;
	}
}
