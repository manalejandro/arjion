package com.manalejandro.arjion.vo;

import java.util.ArrayList;
import java.util.List;

import com.manalejandro.arjion.model.Archivo;
import com.manalejandro.arjion.model.Documento;

public class DocumentoVO {

    private List<Archivo> archivos = new ArrayList<Archivo>();
    private long count;
    private List<Documento> documentos = new ArrayList<Documento>();

    /**
     * @return the archivos
     */
    public List<Archivo> getArchivos() {
        return archivos;
    }

    /**
     * @param archivos the archivos to set
     */
    public void setArchivos(List<Archivo> archivos) {
        this.archivos = archivos;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    /**
     * @return the documentos
     */
    public List<Documento> getDocumentos() {
        return documentos;
    }

    /**
     * @param documentos the documentos to set
     */
    public void setDocumentos(List<Documento> documentos) {
        this.documentos = documentos;
    }
}
