package com.manalejandro.arjion.vo;

import com.manalejandro.arjion.model.Archivo;

public class DetailVO {

    private Archivo archivo;

    /**
     * @return the archivo
     */
    public Archivo getArchivo() {
        return archivo;
    }

    /**
     * @param archivo the archivo to set
     */
    public void setArchivo(Archivo archivo) {
        this.archivo = archivo;
    }
}