package com.manalejandro.arjion.repositories;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.manalejandro.arjion.model.Documento;

@Repository
public interface MainRepository extends ElasticsearchRepository<Documento, String> {

}
