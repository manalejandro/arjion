package com.manalejandro.arjion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArjionApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArjionApplication.class, args);
	}
}
