package com.manalejandro.arjion.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.manalejandro.arjion.model.Consulta;
import com.manalejandro.arjion.model.Documento;
import com.manalejandro.arjion.repositories.MainRepository;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.suggest.Suggest.Suggestion.Entry;
import org.elasticsearch.search.suggest.Suggest.Suggestion.Entry.Option;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class MainServiceImpl implements MainService {

    private final ApplicationContext appContext;
    private final MainRepository mainRepository;

    @Value("#{@indexName}")
    private String index;
    @Value("#{@documentType}")
    private String document;

    @Autowired
    public MainServiceImpl(MainRepository mainRepository, ApplicationContext appContext) {
        this.mainRepository = mainRepository;
        this.appContext = appContext;
    }

    @Override
    public boolean save(Documento doc) {
        if (!mainRepository.existsById(doc.nombre)) {
            if (mainRepository.save(doc) != null)
                return true;
            else
                return false;
        } else
            return false;
    }

    @Override
    public long count() {
        return mainRepository.count();
    }

    @Override
    public List<Documento> findAllDocumento() {
        List<Documento> docs = new ArrayList<Documento>();
        mainRepository.findAll().forEach(doc -> {
            docs.add((Documento) doc);
        });
        return docs;
    }

    @Override
    public Documento findOne(String nombre) {
        return mainRepository.findById(nombre).get();
    }

    @Override
    public Integer maxTamano() {
        return mainRepository.findAll(new Sort(Sort.Direction.DESC, "tamano")).iterator().next().getTamano();
    }

    @Override
    public Consulta search(String busqueda, String[] tipo, Integer tamano, Pageable pageable) {
        Client client = (Client) appContext.getBean("client");
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        if (busqueda != null && !"null".equals(busqueda) && !busqueda.isEmpty()) {
            boolQueryBuilder.must(QueryBuilders.matchQuery("nombre", busqueda));
            boolQueryBuilder.should(QueryBuilders.matchQuery("contenido", busqueda));
        }
        if (tipo != null && tipo.length > 0)
            boolQueryBuilder.filter(QueryBuilders.termsQuery("tipo", tipo));
        if (tamano != null && tamano >= 0)
            boolQueryBuilder.must(QueryBuilders.rangeQuery("tamano").to(tamano).includeUpper(true));
        AggregationBuilder aggregation = AggregationBuilders.terms("by_xarchivo").field("x_archivo").size(10000);
        SuggestBuilder suggest = new SuggestBuilder()
                .addSuggestion("suggest", SuggestBuilders.completionSuggestion("nombre").text(busqueda).size(10))
                .addSuggestion("phrase", SuggestBuilders.phraseSuggestion("nombre").text(busqueda).size(1)
                        .realWordErrorLikelihood((float) 0.95).maxErrors((float) 0.5).gramSize(2));
        System.out.println(boolQueryBuilder);
        SearchResponse response = client.prepareSearch(index).setQuery(boolQueryBuilder).addAggregation(aggregation)
                .suggest(suggest).setSize(pageable.getPageSize()).setFrom(pageable.getPageNumber()).execute()
                .actionGet();
        Consulta consulta = new Consulta();
        consulta.setSuggest(response.getSuggest().getSuggestion("phrase").getEntries().get(0).getOptions().size() > 0
                ? response.getSuggest().getSuggestion("phrase").getEntries().get(0).getOptions().get(0).getText()
                        .string()
                : "");
        for (Entry<? extends Option> entry : response.getSuggest().getSuggestion("suggest").getEntries()) {
            entry.getOptions().forEach(option -> {
                String suggestText = option.getText().string().trim(),
                        autocompleteClean = busqueda.replaceAll("[^\\p{Alnum}\\p{IsAlphabetic} ]", "");
                for (String item : autocompleteClean.split(" ")) {
                    if (item.length() > 0) {
                        consulta.getAutocomplete().add(
                                suggestText.replaceAll("(?i)((?!<)" + item + "(?![^<>]*>))", "<strong>$1</strong>"));
                    }
                }
            });
        }
        ObjectMapper mapper = new ObjectMapper();
        try {
			consulta.setDocumentos(mapper.readValue(response.getHits().getHits().toString(), List.class));
		} catch (IOException e) {
			e.printStackTrace();
		}
        return consulta;
    }
}
