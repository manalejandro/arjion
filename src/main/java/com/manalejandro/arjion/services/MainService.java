package com.manalejandro.arjion.services;

import java.util.List;

import com.manalejandro.arjion.model.Consulta;
import com.manalejandro.arjion.model.Documento;

import org.springframework.data.domain.Pageable;

public interface MainService {

    public boolean save(Documento doc);

    public long count();

    public List<Documento> findAllDocumento();

    public Documento findOne(String nombre);

    public Integer maxTamano();

    public Consulta search(String busqueda, String[] tipo, Integer tamano, Pageable pageable);
}
