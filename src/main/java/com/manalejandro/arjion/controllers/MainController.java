package com.manalejandro.arjion.controllers;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.manalejandro.arjion.model.Archivo;
import com.manalejandro.arjion.model.Documento;
import com.manalejandro.arjion.services.MainService;
import com.manalejandro.arjion.vo.DetailVO;
import com.manalejandro.arjion.vo.DocumentoVO;

import org.apache.tika.config.TikaConfig;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.language.LanguageIdentifier;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.ocr.TesseractOCRConfig;
import org.apache.tika.parser.pdf.PDFParserConfig;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

@Controller
public class MainController {

    private final MainService mainService;

    @Value("${arjion.uploadpath}")
    private String uploadpath;

    @Value("${arjion.tesseractpath}")
    private String tesseractpath;

    @Value("${arjion.tesseractdatapath}")
    private String tesseractdatapath;

    @Autowired
    public MainController(MainService mainService) {
        this.mainService = mainService;
    }

    @RequestMapping(path = "/")
    public String indexPage(final Model model) {
        DocumentoVO documentoVO = new DocumentoVO();
        documentoVO.setCount(mainService.count());
        documentoVO.setDocumentos(mainService.findAllDocumento());
        model.addAttribute("documentoVO", documentoVO);
        return "index";
    }

    @GetMapping(path = "/upload")
    public String upload() {
        return "redirect:/";
    }

    @PostMapping(path = "/upload")
    public String uploadPage(final Model model, @RequestParam("archivos") MultipartFile[] archivos)
            throws IOException, TikaException, SAXException {
        DocumentoVO documentoVO = new DocumentoVO();
        documentoVO.setCount(mainService.count());
        documentoVO.setDocumentos(mainService.findAllDocumento());
        if (archivos.length > 0) {
            // Recupera la configuración de Tika
            TikaConfig tikaConfig = TikaConfig.getDefaultConfig();
            // Itera los archivos recibidos
            for (int i = 0; i < archivos.length; i++) {
                byte[] bytes = archivos[i].getBytes();
                // Normaliza el título de los archivos
                String normalized = Normalizer.normalize(archivos[i].getOriginalFilename(), Normalizer.Form.NFD),
                        filename = normalized.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
                Path path = Paths.get(uploadpath + filename);
                // Instancias necesarias
                Metadata metadata = new Metadata();
                Parser parser = new AutoDetectParser(tikaConfig);
                PDFParserConfig pdfConfig = new PDFParserConfig();
                TesseractOCRConfig tesseractConfig = new TesseractOCRConfig();
                tesseractConfig.setTesseractPath(tesseractpath);
                tesseractConfig.setTessdataPath(tesseractdatapath);
                tesseractConfig.setLanguage("spa+eng");
                pdfConfig.setExtractInlineImages(true);
                ParseContext parseContext = new ParseContext();
                parseContext.set(TesseractOCRConfig.class, tesseractConfig);
                parseContext.set(PDFParserConfig.class, pdfConfig);
                // Usa -1 para no tener límite de 100000 chars
                ContentHandler handler = new BodyContentHandler(-1);
                // Castea los bytes al Stream de Tika
                TikaInputStream stream = TikaInputStream.get(bytes);
                // Parsea el contenido
                parser.parse(stream, handler, metadata, parseContext);
                // Identifica el idioma del archivo
                LanguageIdentifier identifier = new LanguageIdentifier(handler.toString());
                // Almacena en elasticsearch
                String[] names = metadata.names();
                Map<String, String> meta = new HashMap<String, String>();
                for (int j = 0; j < names.length; j++) {
                    meta.put(names[j], metadata.get(names[j]));
                }
                ObjectMapper mapper = new ObjectMapper();
                if (!mainService.save(new Documento(filename, Long.valueOf(archivos[i].getSize()).intValue(),
                        mapper.valueToTree(meta), handler.toString(), identifier.getLanguage()))) {
                    return "exists";
                } else {
                    // Guarda el archivo en el directorio configurado en las properties
                    Files.write(path, bytes);
                }
                // Añade los parámetros al VO para mostrar en la vista
                documentoVO.getArchivos().add(new Archivo(filename, Long.valueOf(archivos[i].getSize()).intValue(), meta,
                        handler.toString(), identifier.getLanguage()));
            }
        }
        model.addAttribute("documentoVO", documentoVO);
        return "index";
    }

    @GetMapping(path = "/detail")
    public String detail(final Model model, @RequestParam(value = "nombre", required = true) String nombre) {
        DetailVO detailVO = new DetailVO();
        ObjectMapper mapper = new ObjectMapper();
        Documento doc = mainService.findOne(nombre);
        detailVO.setArchivo(new Archivo(doc.getNombre(), doc.getTamano(),
                mapper.convertValue(doc.getMetadata(), Map.class), doc.getContenido(), doc.getLenguaje()));
        model.addAttribute("detailVO", detailVO);
        return "detail";
    }

    @GetMapping(path = "/download")
    public ResponseEntity<ByteArrayResource> download(final HttpServletResponse response,
            @RequestParam(value = "filename", required = true) String filename)
            throws IOException, MalformedURLException {
        File file = new File(uploadpath + filename);
        Path path = Paths.get(file.getAbsolutePath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
        String type = file.toURL().openConnection().guessContentTypeFromName(filename);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Disposition", "attachment; filename=" + URLEncoder.encode(filename, "UTF-8"));
        responseHeaders.add("Content-Type", type);
        return ResponseEntity.ok().contentLength(file.length()).headers(responseHeaders).body(resource);
    }
}