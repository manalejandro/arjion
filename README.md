## Arjion

### del griego `archivo`

### Proof of Concept with [SpringBoot 2.1.0](https://start.spring.io/), [ElasticSearch](https://www.elastic.co/) and [Apache Tika](https://tika.apache.org/)

## Docker image

    $ docker build -t debian:arjion --rm https://gitlab.com/manalejandro/arjion/raw/master/Dockerfile
    $ docker run -ti -p 8080:8080 debian:arjion

## License

MIT